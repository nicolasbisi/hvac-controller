Pin 9 para o emissor
Pin 11 para o receptor

sempre enviar o codigo Infra vermelho 2x
ip atual 192.168.1.25

Exemplos de url ( set/mode/valorTemp/ ):

http://192.168.1.25/set/auto/0/
http://192.168.1.25/set/off/0/
http://192.168.1.25/set/wind/18/
http://192.168.1.25/set/heat/18/

http://192.168.1.25/get/

Obs: eh importante colocar o / no final de tudo, para filtrar com o resto do protocolo http.
Obs2: 192.168.1.25/set/heat/18/ eh suficiente, n precisa do http://


A ideia eh que o app (web ou mobile) faça uma requisição do tipo get ( http://192.168.1.25/get/ ) na inicialização e depois a cada x tempo (5, ou 10s).
Esse retorno vai ser uma string  temp/humidade/


Ideias:
No boot do arduino setar AC em Off
Setar horário fixo para pessoa chegar em casa, e ligar o AC meia hora antes
Adivinhar um padrao no comportamento da pessoa para ligar meia hora antes
Ter um trigger no trabalho da pessoa para ligar o AC automaticamente
Ter vários sensores de presença na casa para ligar/desligar AC