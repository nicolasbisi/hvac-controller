

#include <IRremote.h>
int RECV_PIN = 11;  //Receiver
IRrecv irrecv(RECV_PIN);
decode_results results;
unsigned int rawCodes[RAWBUF];
int codeLen; // The length of the code

void setup(){
  Serial.begin(9600);
  irrecv.enableIRIn(); // Start the receiver
}

void loop() {

  if (irrecv.decode(&results)) {
    storeCode(&results);
    irrecv.resume(); // resume receiver
  }
}

void storeCode(decode_results *results) {
    Serial.println("Raw code received with its lengh:");
    codeLen = results->rawlen - 1;
    Serial.print("{");
    for (int i = 1; i <= codeLen; i++) {
      rawCodes[i - 1] = results->rawbuf[i]*USECPERTICK - MARK_EXCESS;
      Serial.print(",");
      Serial.print(rawCodes[i - 1], DEC);
    }
    Serial.println("}");
    Serial.println(codeLen);
}
