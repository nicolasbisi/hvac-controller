//Pin 9 para o emissor para Mega e pin 3 para o Uno
//Pin 11 para o receptor
//sempre enviar o codigo Infra vermelho 2x
//ip atual 192.168.1.25
//
//Exemplos de url ( set/mode/valorTemp/ ):
//  http://192.168.1.25/set/auto/0/
//  http://192.168.1.25/set/off/0/
//  http://192.168.1.25/set/wind/18/
//  http://192.168.1.25/set/heat/18/

// Obs: eh importante colocar o / no final de tudo, para filtrar com o resto do protocolo http.

#include <IRremote.h>
#include <Ethernet.h>
#include <SPI.h>        //necessario para comunicacao ethernet
#include <dht.h>        //sensor de umidade e temperatura


// Variaveis IR
IRsend irsend;

//Code20 e code22 serao apagados. Estao aqui soh pq seus valores ja foram capturados, e podem ser necessarios para futuros testes
unsigned int code20[] = {4300,4150,550,1400,550,400,450,1450,500,1450,500,400,500,400,500,1450,500,400,500,350,500,1400,550,400,500,400,450,1450,550,1400,550,400,450,1450,500,400,500,400,500,400,450,1450,500,1450,550,1400,550,1450,500,1400,550,1400,550,1450,500,1400,550,400,500,400,450,400,500,400,500,400,450,400,500,400,500,1400,550,400,450,400,500,1400,550,400,500,400,450,1450,500,1450,500,400,500,1450,500,1450,500,400,500,1400,550,1450,500};
unsigned int code22[] = {4350,4100,550,1450,500,400,500,1400,550,1400,550,400,500,400,450,1450,500,400,500,400,500,1450,500,400,450,400,500,1450,500,1450,500,400,500,1450,500,400,500,400,450,400,500,1450,500,1450,500,1450,500,1450,500,1450,500,1450,500,1450,550,1400,550,400,450,400,500,400,500,400,450,400,500,400,450,1450,550,1400,550,1450,500,400,450,1450,500,450,450,400,500,1400,550,400,450,450,450,400,500,1450,500,400,450,1500,500,1400,550};

unsigned int off[] = {2600,4000,4350,4100,550,1400,550,350,550,1400,550,1400,550,350,550,350,500,1400,550,400,500,350,550,1400,550,350,500,400,500,1400,550,1400,550,400,500,1400,550,400,450,1450,500,1450,550,1400,550,1400,550,400,450,1450,500,1450,550,1400,550,400,450,400,500,400,450,400,500,1450,500,400,500,400,450,1450,550,1400,550,1400,550,400,450,400,500,400,500,400,450,400,500,400,500,400,450,400,500,1450,500,1450,500,1450,500,1450,500};

unsigned int wind18[] = {100};//exemplo
unsigned int wind19[] = {100};//exemplo
unsigned int wind20[] = {100};//exemplo
unsigned int wind21[] = {100};//exemplo
unsigned int wind22[] = {100};//exemplo
unsigned int wind23[] = {100};//exemplo
unsigned int wind24[] = {100};//exemplo
unsigned int wind25[] = {100};//exemplo
unsigned int wind26[] = {100};//exemplo
unsigned int wind27[] = {100};//exemplo
unsigned int wind28[] = {100};//exemplo

unsigned int heat18[] = {100};//exemplo
unsigned int heat19[] = {100};//exemplo
unsigned int heat20[] = {100};//exemplo
unsigned int heat21[] = {100};//exemplo
unsigned int heat22[] = {100};//exemplo
unsigned int heat23[] = {100};//exemplo
unsigned int heat24[] = {100};//exemplo
unsigned int heat25[] = {100};//exemplo
unsigned int heat26[] = {100};//exemplo
unsigned int heat27[] = {100};//exemplo
unsigned int heat28[] = {100};//exemplo

int codeLen = 99;

// Variaveis temperatura e humidade
#define tempHumiPin A0
dht DHT;

//Variaveis do AC
String mode = "off/"; //Assumindo que o AC esteja desligado (falha de energia) quando o Arduino eh ligado
String temp = "0/";
#define NO 0
#define COOLING 1
#define HEATING 2

//Variaveis ethernet
byte mac[] = { 0xDE, 0xBE, 0xAD, 0xFE, 0xEF, 0xED }; //physical mac address
byte ip[] = { 192, 168, 1, 25 }; // ip in lan
byte gateway[] = { 192, 168, 1, 1 }; // internet access via router
byte subnet[] = { 255, 255, 255, 0 }; //subnet mask
EthernetServer server(80); //server port
String input = "";

//Variaveis auto

int threshold = 3;
int tempDesejada; //temp em formato int para poder comparar com threshold
int isAdjusting = 0; //flag to know that the AC is already trying to adjust to auto

void setup(){
  Ethernet.begin(mac, ip, gateway, subnet); //Inicializando ethernet
  server.begin();
  Serial.begin(9600); //inicializando comunicacao serial para debug
  Serial.flush();
  
  Serial.println("HVAC Controller - v0.1");
}

void loop() {
  input = "";
  DHT.read11(tempHumiPin);
  EthernetClient client = server.available(); 
  if (client) {
    while (client.connected()) {
      if (client.available()) {       
        char c = client.read();
        if(c == 'G'){             //Realiza filtro da requesições do aplicativo
          c = client.read();      //como o arduino recebe caracter a caracter, eh
          if(c == 'E'){           //mais rapido tratar a sequencia de caracteres do que
            c = client.read();    //juntar tudo em uma string para depois manipular.
            if(c == 'T'){
              c = client.read();
              if(c == ' '){
                c = client.read();
                if(c == '/'){
                  do{
                    c = client.read();
                    input = input+c;    //Agrupa caracteres para decidir se eh set/ ou get/
                  }while (c != '0');
                  input = decrypt(input);
                  if(input == "set/"){ 
                    input = "";
                    do{
                      c = client.read();
                      input = input+c;  //Agrupa caracteres para decidir o mode
                    }while (c != '0');
                    input = decrypt(input);
                    mode = input;
                    input = "";
                    do{
                      c = client.read();
                      input = input+c;
                    }while (c != '0');
                      input = decrypt(input);
                      temp = input;
                      set(mode, temp);
                      client.println(encrypt(ambientAndAcState())); //Retorna para o cliente temp/humid/mode/temp/
                  } 
                  if (input == "get/"){
                    client.println(encrypt(ambientAndAcState())); //Retorna para o cliente temp/humid/mode/temp/
                  }
                }
              }
            }
          }
        }
      }
    client.println(""); //Http protocol ends with an empty line
    client.stop();
    }   
  }

  //Automatico
  if (mode == "auto/" && isAdjusting == NO){
    //refrigerar
    if( (int)DHT.temperature - tempDesejada >threshold) { //temperatura ambiente - temperatura desejada > threshold -> refrigerar Ex: desejado = 20. Acionaria com 23 ou mais
      acWind(String(tempDesejada-2)+'/'); //mandar refrigerar 2 graus a menos do desejado (apenas para ser mais rapido)
      isAdjusting = COOLING;
      return;
    }
    //aquecer
    if( tempDesejada - (int)DHT.temperature >threshold) { //temperatura desejada - temperatura ambiente  > threshold -> aquecer Ex: desejado = 20. Acionaria com 17 ou menos
      acHeat(String(tempDesejada+2)+'/'); //mandar aquecer 2 graus a mais do desejado (apenas para ser mais rapido)
      isAdjusting = HEATING;
      return;
    }
  }
  if (mode == "auto/" && isAdjusting == COOLING &&  tempDesejada - (int)DHT.temperature == 1){  //caso esteja em auto, esteja refrigerando, Entao quando temperatura desejada - termometro for == 1, desliga o ar (esta 1 grau abaixo do desejado, oq eh bom em clima quente)
    acOff();
    isAdjusting = NO;
  }
  if (mode == "auto/" && isAdjusting == HEATING && (int)DHT.temperature - tempDesejada  == 1){  //caso esteja em auto, esteja aquecendo, e a temperatura do termometro - desejada for == 1, desliga, pois esta 1 grau acima do desejado (oq eh bom em clima frio)
    acOff();
    isAdjusting = NO;
  }
  if (mode != "auto/" && isAdjusting != NO){ //caso alguem tire do altomatico enquanto ele esta se ajustando, coloca o isAdjusting de volta para 0, para quando colocar em auto novamente nao de problema
    isAdjusting = NO;
  }
}

String ambientAndAcState (){
  return String((int)DHT.temperature) + "/" + String((int) DHT.humidity) + "/" + mode + temp;
}

void set (String mode, String value){
  if (mode == "off/"){
    acOff();
    return;
  }
  if (mode == "auto/"){
    acAuto(value);
    return;
  }
  if (mode == "heat/"){
    acHeat(value);
    return; 
  }
  if (mode == "wind/"){
    acWind(value);
    return; 
  }
}

void acOff (){
  irsend.sendRaw(off, codeLen, 38);
  irsend.sendRaw(off, codeLen, 38);
  Serial.println("off"); //debug
}
void acAuto (String value){
  tempDesejada = (value.substring(0, value.length() - 1)).toInt(); //remove / de valor/ e converte em int
  Serial.print("auto "); //debug
  Serial.println(tempDesejada); //debug
}
void acHeat (String value){
  if (value == "18/"){
      irsend.sendRaw(heat18, codeLen, 38);
      irsend.sendRaw(heat18, codeLen, 38);
      Serial.println("heat 18"); //debug
      return; 
    }
    if (value == "19/"){
      irsend.sendRaw(heat19, codeLen, 38);
      irsend.sendRaw(heat19, codeLen, 38);
      Serial.println("heat 19"); //debug
      return; 
    }
    if (value == "20/"){
      irsend.sendRaw(heat20, codeLen, 38);
      irsend.sendRaw(heat20, codeLen, 38);
      Serial.println("heat 20"); //debug
      return; 
    }
    if (value == "21/"){
      irsend.sendRaw(heat21, codeLen, 38);
      irsend.sendRaw(heat21, codeLen, 38);
      Serial.println("heat 21"); //debug
      return; 
    }
    if (value == "22/"){
      irsend.sendRaw(heat22, codeLen, 38);
      irsend.sendRaw(heat22, codeLen, 38);
      Serial.println("heat 22"); //debug
      return; 
    }
    if (value == "23/"){
      irsend.sendRaw(heat23, codeLen, 38);
      irsend.sendRaw(heat23, codeLen, 38);
      Serial.println("heat 23"); //debug
      return; 
    }
    if (value == "24/"){
      irsend.sendRaw(heat24, codeLen, 38);
      irsend.sendRaw(heat24, codeLen, 38);
      Serial.println("heat 24"); //debug
      return; 
    }
    if (value == "25/"){
      irsend.sendRaw(heat25, codeLen, 38);
      irsend.sendRaw(heat25, codeLen, 38);
      Serial.println("heat 25"); //debug
      return; 
    }
    if (value == "26/"){
      irsend.sendRaw(heat26, codeLen, 38);
      irsend.sendRaw(heat26, codeLen, 38);
      Serial.println("heat 26"); //debug
      return; 
    }
    if (value == "27/"){
      irsend.sendRaw(heat27, codeLen, 38);
      irsend.sendRaw(heat27, codeLen, 38);
      Serial.println("heat 27"); //debug
      return; 
    }
    if (value == "28/"){
      irsend.sendRaw(heat28, codeLen, 38);
      irsend.sendRaw(heat28, codeLen, 38);
      Serial.println("heat 28"); //debug
      return; 
    }
}
void acWind (String value){
  if (value == "18/"){
      irsend.sendRaw(wind18, codeLen, 38);
      irsend.sendRaw(wind18, codeLen, 38);
      Serial.println("wind 18"); //debug
      return; 
    }
    if (value == "19/"){
      irsend.sendRaw(wind19, codeLen, 38);
      irsend.sendRaw(wind19, codeLen, 38);
      Serial.println("wind 19"); //debug
      return; 
    }
    if (value == "20/"){
      irsend.sendRaw(wind20, codeLen, 38);
      irsend.sendRaw(wind20, codeLen, 38);
      Serial.println("wind 20"); //debug
      return; 
    }
    if (value == "21/"){
      irsend.sendRaw(wind21, codeLen, 38);
      irsend.sendRaw(wind21, codeLen, 38);
      Serial.println("wind 21"); //debug
      return; 
    }
    if (value == "22/"){
      irsend.sendRaw(wind22, codeLen, 38);
      irsend.sendRaw(wind22, codeLen, 38);
      Serial.println("wind 22"); //debug
      return; 
    }
    if (value == "23/"){
      irsend.sendRaw(wind23, codeLen, 38);
      irsend.sendRaw(wind23, codeLen, 38);
      Serial.println("wind 23"); //debug
      return; 
    }
    if (value == "24/"){
      irsend.sendRaw(wind24, codeLen, 38);
      irsend.sendRaw(wind24, codeLen, 38);
      Serial.println("wind 24"); //debug
      return; 
    }
    if (value == "25/"){
      irsend.sendRaw(wind25, codeLen, 38);
      irsend.sendRaw(wind25, codeLen, 38);
      Serial.println("wind 25"); //debug
      return; 
    }
    if (value == "26/"){
      irsend.sendRaw(wind26, codeLen, 38);
      irsend.sendRaw(wind26, codeLen, 38);
      Serial.println("wind 26"); //debug
      return; 
    }
    if (value == "27/"){
      irsend.sendRaw(wind27, codeLen, 38);
      irsend.sendRaw(wind27, codeLen, 38);
      Serial.println("wind 27"); //debug
      return; 
    }
    if (value == "28/"){
      irsend.sendRaw(wind28, codeLen, 38);
      irsend.sendRaw(wind28, codeLen, 38);
      Serial.println("wind 28"); //debug
      return; 
    }
}


String encrypt (String input){
  int inputLenght  = input.length()+1;
  char buffer[inputLenght];
  input.toCharArray(buffer, inputLenght);
  for (int x=0; x< inputLenght-1; x++){
    if (buffer[x] == 'z'){
      buffer[x] = '&';
    }
    else{
      buffer[x]++;
  }
  }
  return String(buffer);
}

String decrypt (String input){
  int inputLenght  = input.length()+1;
  char buffer[inputLenght];
  input.toCharArray(buffer, inputLenght);
  for (int x=0; x< inputLenght-1; x++){
    if (buffer[x] == '&'){
      buffer[x] = 'z';
    }
    else{
      buffer[x]--;
  }
  }
  return String(buffer);
}


